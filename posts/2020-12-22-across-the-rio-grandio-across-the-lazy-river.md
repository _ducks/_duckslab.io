---
title: "Across the Rio Grandio, across the lazy river"
published_date: "2020-12-22 23:27:12 +0000"
layout: default.liquid
is_draft: false
data:
  date: "October 15, 1973"
  preview: "If all you got to live for is what you left behind, get yourself a 
  powder charge and seal that silver mine"
---
# Across the Rio Grandio, across the lazy river

On the day when I was born, Daddy sat down and cried  
I had the mark just as plain as day which could not be denied  
They say that Cain caught Abel rolling loaded dice  
Ace of Spades behind his ear and him not thinking twice  

Half-step, Mississippi uptown toodeloo  
Hello, baby, I'm gone, goodbye  
Half a cup of rock and rye  
Farewell to you old Southern skies  
I'm on my way, on my way  
