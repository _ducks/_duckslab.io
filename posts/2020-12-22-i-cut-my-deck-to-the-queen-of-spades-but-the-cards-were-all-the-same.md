---
title: "I cut my deck to the Queen of Spades, but the cards were all the same"
published_date: "2020-12-22 23:14:55 +0000"
layout: default.liquid
is_draft: false
data:
  date: "June 7, 1969"
  preview: "In the timbers of Fennario, the wolves were runnin' round. The
  winter was so hard and cold froze ten feet 'neath the ground"
---
# I cut my deck to the Queen of Spades, but the cards were all the same

Don't murder me  
I beg of you, don't murder me  
Please don't murder me  


I sat down to my supper  
It was a bottle of red whiskey  
I said my prayers and went to bed  
That's the last they saw of me  


Don't murder me  
I beg of you, don't murder me  
Please don't murder me  


When I awoke, the dire wolf  
Six hundred pounds of sin  
Was grinning at my window  
All I said was, "Come on in"  
