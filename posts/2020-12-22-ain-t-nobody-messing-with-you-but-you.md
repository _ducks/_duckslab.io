---
title: "Ain't nobody messing with you but you"
published_date: "2020-12-22 23:23:34 +0000"
layout: default.liquid
is_draft: false
data:
  preview: "I told Althea I was feeling lost, lacking in some direction. Althea
  told me up scrutiny, my back may need some protection."
  date: "April 28, 1980"
---
# Ain't nobody messing with you but you 

I told Althea that treachery  
Was tearing me limb from limb  
Althea told me: now cool down boy  
Settle back easy Jim  


You may be Saturday's child all grown  
Moving with a pinch of grace  
You may be a clown in the burying ground  
Or just another pretty face  
You may be the fate of Ophelia  
Sleeping and perchance to dream  
Honest to the point of recklessness  
Self centered to the extreme  
Ain't nobody messing with you but you  
Your friends are getting most concerned  
Loose with the truth  
Maybe it's your fire  
But baby...don't get burned  
