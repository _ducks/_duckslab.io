---
title: This is the title
published_date: "2020-12-22 10:43:35 +0000"
layout: default.liquid
is_draft: false
data:
  date: "January 1, 1900"
  preview: "A few sentences will go here to give a preview of the post. 
  Talk about what's in the post or some clever saying."
---
# This is our first Post!

Welcome to the first post ever on cobalt.rs!
