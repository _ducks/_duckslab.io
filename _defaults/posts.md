---
layout: default.liquid

title: Post title was here 
data: {
  date: "January 1, 1900",
  preview: "A few sentences will go here to give a preview of the post. Talk
            about what's in the post and I'm having a high time living the good
            life"
}
---

# This is our first Post!

Welcome to the first post ever on cobalt.rs!
